/*
***************************************************************
***************************************************************

    Theme Name  : JFOLIO | One Page Portfolio Template
    Author      : Golam
    Author URI  : http://themeforest.net/user/templatehut
    File        : Active.js

***************************************************************
***************************************************************/

(function ($) {
    "use strict";

    var allFunctions = {
        $window: $(window),
        customFunction: {
            init: function () {
                allFunctions.customFunction.skills(),
                    allFunctions.customFunction.sectionScroll(),
                    allFunctions.customFunction.scrollToTop(),
                    allFunctions.customFunction.navBar();
            },
            skills: function () {
                var startSkills = $('.single-skill');
                startSkills.waypoint(function () {
                    $(this).each(function () {
                        var data = $(this).data('percent');
                        $(this).find('.skill-bar-overlay').animate({
                            width: data + "%"
                        }, 2000);

                        $(this).find('span').addClass('show').animate({
                            width: data + (-20) + "%"
                        }, 2000);
                    });
                }, {
                    triggerOnce: true,
                    offset: 'bottom-in-view'
                });

            },
            sectionScroll: function () {

                var scroll = $(".section-scroll");
                scroll.on("click", function (e) {
                    var $anchor = $(this),
                        offsetTop = 100;
                    $('html, body').stop().animate({
                        scrollTop: $($anchor.attr('href')).offset().top - offsetTop + "px"
                    }, 1200, 'easeInOutExpo');
                    e.preventDefault();

                });

                var body = $('body');
                body.scrollspy({
                    target: '.navbar-collapse',
                    offset: 95
                });

                $(document).on('click', '.navbar-collapse.in', function (e) {
                    if ($(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle') {
                        $(this).collapse('hide');
                    }
                });

            },
            scrollToTop: function () {
                $(window).on('scroll', function () {
                    var scrollTop = $(this).scrollTop();
                    if (scrollTop > 400) {
                        $('a.scroll-to-top').fadeIn('slow');
                    } else {
                        $('a.scroll-top').fadeOut('slow');
                    }
                });

                $('.scroll-to-top').on('click', function () {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1000, 'easeInOutExpo');

                    return false;
                });
            },
            navBar: function () {
                $(window).on('scroll', function () {
                    var navScrollTop = $(this).scrollTop();
                    if (navScrollTop > 200) {
                        $('.mainmenu-area').addClass('nav-fixed animated fadeInDown');
                    } else {
                        $('.mainmenu-area').removeClass('nav-fixed animated fadeInDown');
                    }
                });
            }
        },
        pluginFunction: {
            init: function () {
                allFunctions.pluginFunction.parallax(),
                allFunctions.pluginFunction.popup();
            },
            portfolioFilter: function () {
                var $grid = $('.grid').isotope({
                    itemSelector: '.item',
                    percentPosition: true,
                    masonry: {
                        // use outer width of grid-sizer for columnWidth
                        columnWidth: 1
                    }
                });

                // filter items on button click
                $('.portfolio-menu').on('click', 'li', function () {
                    var filterValue = $(this).attr('data-filter');
                    $grid.isotope({
                        filter: filterValue
                    });
                });

                $('.portfolio-menu').each(function (i, buttonGroup) {
                    var $buttonGroup = $(buttonGroup);
                    $buttonGroup.on('click', 'li', function () {
                        $buttonGroup.find('.current').removeClass('current');
                        $(this).addClass('current');
                    });
                });
            },
            parallax: function () {
                allFunctions.$window.stellar({
                    horizontalScrolling: false,
                    positionProperty: 'position',
                    responsive: true
                });
            },
            popup: function () {
                if ($.fn.magnificPopup) {
                    var popup = $(".portfolio-popup");
                    popup.magnificPopup({
                        type: 'image',
                        removalDelay: 300,
                        mainClass: 'mfp-with-zoom',
                        gallery: {
                            enabled: true
                        },
                        zoom: {
                            enabled: true, // By default it's false, so don't forget to enable it

                            duration: 300, // duration of the effect, in milliseconds
                            //easing: 'ease-in-out', // CSS transition easing function

                            // The "opener" function should return the element from which popup will be zoomed in
                            // and to which popup will be scaled down
                            // By defailt it looks for an image tag:
                            opener: function (openerElement) {
                                // openerElement is the element on which popup was initialized, in this case its <a> tag
                                // you don't need to add "opener" option if this code matches your needs, it's defailt one.
                                return openerElement.is('a') ? openerElement : openerElement.find('a');
                            }
                        }
                    });
                }
            }
        },
        sliderFunction: {
            init: function () {
                allFunctions.sliderFunction.educationSlider(),
                    allFunctions.sliderFunction.experienceSlider(),
                    allFunctions.sliderFunction.testimonialSlider();
            },
            educationSlider: function () {
                if ($.fn.owlCarousel) {
                    var educationSlider = $(".education-wrap");
                    educationSlider.owlCarousel({
                        items: 1, // Default is 3
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000, // Default is 5000
                        smartSpeed: 3000, // Default is 250
                        dots: true,
                        dotsEach: true,
                        nav: true,
                        navText: [
							'<i class="fa fa-arrow-circle-o-left"></i>',
							'<i class="fa fa-arrow-circle-o-right"></i>'
							]
                    });
                }
            },
            experienceSlider: function () {
                if ($.fn.owlCarousel) {
                    var experienceSlider = $(".experience-wrap");
                    experienceSlider.owlCarousel({
                        items: 1, // Default is 3
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 3000, // Default is 5000
                        smartSpeed: 250, // Default is 250
                        dots: true,
                        dotsEach: true,
                        nav: true,
                        navText: [
							'<i class="fa fa-arrow-circle-o-left"></i>',
							'<i class="fa fa-arrow-circle-o-right"></i>'
							]
                    });
                }
            },
            testimonialSlider: function () {
                if ($.fn.owlCarousel) {
                    var testimonialSlider = $(".testimonial-wrap");
                    testimonialSlider.owlCarousel({
                        items: 1, // Default is 3
                        loop: true,
                        autoplay: true,
                        autoplayTimeout: 6000, // Default is 5000
                        smartSpeed: 1000, // Default is 250
                        dots: false,
                        nav: true,
                        navText: [
							'<i class="fa fa-angle-left"></i>',
							'<i class="fa fa-angle-right"></i>'
							]
                    });
                }
            }
        },
        ajaxForms: {
            ajaxFormHandle: function (f) {
                f.on("submit", function (e) {
                    e.preventDefault();
                    var a = $(this),
                        b = a.find('msg-box');
                    $.ajax({
                        url: a.attr('action'),
                        type: 'POST',
                        data: a.serialize(),
                        success: function (data) {
                            setTimeout(function () {
                                $(".msg-box").append('<div class="alert alert-msg alert-dismissible" role="alert">' + data + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><i class="fa fa-times"></i></button></div>');
                                $(".form-control").val('');
                            }, 200 * f);
                        }
                    });
                });
            },
            contact: function () {
                $(".contact-form-wrap").each(function () {
                    allFunctions.ajaxForms.ajaxFormHandle($(this).find('form'));
                });
            }
        }
    }

    $(window).on('load', function () {
        allFunctions.pluginFunction.portfolioFilter();

    });

    $(document).ready(function () {
        allFunctions.customFunction.init();
        allFunctions.pluginFunction.init();
        allFunctions.sliderFunction.init();
        allFunctions.ajaxForms.contact();

    });

}(jQuery));