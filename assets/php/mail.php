<?php

	// Get Vale From Input First Name
	$name = trim($_POST['name']);
	$name = strip_tags(htmlspecialchars($name));
	$name = str_replace(array("\r","\n"),array(" "," "),$name);
	
	
	// Get Vale From Input Name Subject
	$subject = trim($_POST['subject']);
	$subject = strip_tags(htmlspecialchars($subject));
	
	// Get Vale From Input Name Email
	$email = trim($_POST['email']);
	$email = filter_var($email, FILTER_SANITIZE_EMAIL);
	
	// Get Vale From Input Name Msg
	$msg = trim($_POST['msg']);
	$msg = strip_tags(htmlspecialchars($msg));
	
	
	// Check if Vale is Empty then Exit
	if ( empty($name) || empty($subject) || empty($email) || empty($msg) ){
		echo "Por favor, preencha todos os campos!";
	}
    elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)){
        echo "Endereço de email inválido!";
    }
    else{
	
		$mail_to = "contato@antoniorolim.com.br"; 	// Change Your Email Address Here

		
		
		$email_subject  = "Contato site ". $name;
		
		$msg_body  = "Nome: ".$name."\r\n";
		$msg_body .= "Assunto: ".$subject."\n";
		$msg_body .= "Email: ".$email."\n\n";
		$msg_body .= "Mensagem:\n".$msg."\n";
		$email_headers  = "De <" .$email. "> ";
		
		if(mail($mail_to, $email_subject, $msg_body, $email_headers)){
			
			echo " Obrigado, sua mensagem foi enviada com sucesso!";
		}else{
			
			echo " Oops! Ocorreu algum problema, tente enviar mais tarde sua mensagem";
		}
	}
	
