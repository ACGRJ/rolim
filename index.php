<!DOCTYPE html>
<html lang="en">

<head>
    <!--   [Meta Tags]-->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <!--    [Site Title] -->
    <title>Antonio Rolim</title>

    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">

    <!--    [Bootstrap Css] -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!--    [FontAwesome Css] -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!--    [Owl Carousel Css] -->
    <link href="assets/css/owl.carousel.min.css" rel="stylesheet">
    <!--    [Animate Css] -->
    <link href="assets/css/animate.css" rel="stylesheet">
    <!--    [Magnific Popup Css] -->
    <link href="assets/css/magnific-popup.min.css" rel="stylesheet">
    <!--    [Custom Css]-->
    <link href="assets/css/style.css" rel="stylesheet">
    <link href="assets/css/responsive.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>s
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>

    <!--    [ START PRLOADER AREA]-->
    <div class="site-preloader-area">
        <div class="site-preloader"></div>
    </div>
    <!--    [FINISH PRLOADER AREA]-->

    <!--    [Start Header Area] -->
    <header class="header-area" id="home">
        <!--    [header top] -->
        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-sm-6 selima" style="font-size: 40px;">
                       <!-- Antonio Rolim-->
                    </div>
                    <div class="col-md-4 col-sm-6 text-right">
                        <ul class="social-profile">
                            <li><a href="https://www.facebook.com/antoniorolim96"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="https://twitter.com/Antonio_Rolim"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="https://github.com/antoniorolim"><i class="fa fa-github"></i></a></li>
                            <li><a href="https://www.linkedin.com/in/antonio-rolim-67094654/?lipi=urn%3Ali%3Apage%3Ad_flagship3_feed%3B49yEi6TNQjWoH8Ok6dvUPQ%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_feed-identity_profile_photo"><i class="fa fa-linkedin"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <!--    [ End header top]-->

        <!--    [Start Menu Area]-->
        <div class="mainmenu-area">
            <div class="container">
                <div class="row">
                    <!--    [Start Logo]-->
                    <div class="col-md-3 col-sm-12">
                        <div class="logo">
                            <a href="index.html">      
                            	<img src="assets/img/logo.png" alt="logo" style="width: 130px;">                                                          
                            </a>
                        </div>
                    </div>
                    <!--    [ End Logo]-->

                    <!--    [ Start Menu]-->
                    <div class="col-md-9 col-sm-12">
                        <nav class="menu">
                            <div class="navbar">
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                </div>
                                <div class="navbar-collapse collapse">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li class="active"><a class="section-scroll" href="#home">Inicio</a></li>
                                        <li><a class="section-scroll" href="#about">Sobre</a></li>
                                        <li><a class="section-scroll" href="#skills">Habilidades</a></li>
                                        <li><a class="section-scroll" href="#service">Soluções</a></li>
                                        <li><a class="section-scroll" href="#portfolio">Portfólio</a></li>
                                        <li><a class="section-scroll" href="#contact">Contato</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                    <!--    [Finish Menu]-->
                </div>
            </div>
        </div>
        <!--    [ End Menu Area]-->
    </header>
    <!--    [ End Header Area] -->

    <!--    [Start Promo Area]-->
    <section class="promo-area">
        <div class="promo-wrap">
            <div class="single-promo-item promo-bg-1" data-stellar-background-ratio="0.6">
                <div class="promoTable">
                    <div class="promo-tableCell">
                        <div class="container">
                            <div class="row">
                                <div class="col-md-7">
                                    <div class="promo-text">
                                        <h2>Antonio Rolim, prazer!</h2>
                                        <h4 class="cd-headline clip">
                                            Trabalho com
                                            <span class="cd-words-wrapper">
                                                <b class="is-visible">Desenvolvimento Web</b>
                                                <b>Marketing Digital</b>
                                                <b>Soluções de negócios</b>
                                            </span>
                                        </h4>
                                        <p>Não importa qual a necessidade que você encontra hoje, trabalhando duro e com muita dedicação, juntos vamos resolver da maneira mais adequada para o seu empreendimento. </p>
                                        <a href="#about" class="btn portfolio-btn section-scroll">Sobre mim</a>
                                        <a href="#portfolio" class="btn portfolio-btn section-scroll">Veja meus trabalhos</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [ End Promo Area] -->


    <!--    [Start About Me Area] -->
    <section class="about-me-area section-padding" id="about">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="section-title">
                        <h3>Sobre</h3>
                        <p>Programador, 20 anos, formado técnico em informática pelo Instituto Federal do Rio Grande do Sul - Campus Canoas. Apaixonado por artes como canto e dança. Futuro publicitário/influenciador digital, que estuda audio/visual.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <span class="interest">Hobbies e interesses</span>
                <div class="col-md-6">
                    <div class="row">
                        <!--    [Start About Info] -->
                        <div class="col-sm-4">
                            <div class="single-about-info">
                                <i class="fa fa-gamepad"></i> jogos
                            </div>
                        </div>
                        <!--    [End About Info] -->
                        <!--    [Start About Info] -->
                        <div class="col-sm-4">
                            <div class="single-about-info">
                                <i class="fa fa-coffee"></i> café
                            </div>
                        </div>
                        <!--    [End About Info] -->
                        <!--    [Start About Info] -->
                        <div class="col-sm-4">
                            <div class="single-about-info">
                                <i class="fa fa-music"></i> musica
                            </div>
                        </div>
                        <!--    [End About Info] -->
                        <!--    [Start About Info] -->
                        <div class="col-sm-4">
                            <div class="single-about-info">
                                <i class="fa fa-camera"></i> fotografia
                            </div>
                        </div>
                        <!--    [End About Info] -->
                        <!--    [Start About Info] -->
                        <div class="col-sm-4">
                            <div class="single-about-info">
                                <i class="fa fa-film"></i> series
                            </div>
                        </div>
                        <!--    [End About Info] -->
                        <!--    [Start About Info] -->
                        <div class="col-sm-4">
                            <div class="single-about-info">
                                <i class="fa fa-plane"></i> viagem
                            </div>
                        </div>
                        <!--    [End About Info] -->
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="about-details">
                        <div class="about-bar top"></div>
                        <span>Nome :</span> <span class="primary-color">Antonio Rolim</span> <br>
                        <span>Data de Nascimento :</span> 06/09/1996 <br>
                        <span>Telefone :</span> (51) 98467-8003 <br>
                        <span>E-mail :</span> contato@antoniorolim.com.br <br>
                        <a href="assets/downloads/curriculo.pdf" class="btn download-btn"><i class="fa fa-download"></i>Curriculo Completo</a>
                        <div class="about-bar bottom"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [  End About Me Area] -->

    <!--    [Start Education Area] -->
    <section class="education-area section-padding" id="education">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="section-title">
                        <h3>Formação e experiência</h3>
                        <p>Um pouco sobre como cheguei aonde estou!</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="education-wrap owl-carousel">
                    <!--    [Start Single education Info] -->
                    <div class="col-sm-12">
                        <div class="single-education">
                            <div class="qualification">
                                <span class="primary-color"> Tec. informática</span>
                                <span> IFRS - Canoas</span>
                                <span> 2010-2014</span>
                            </div>
                            <p> Uma formação ampla e complexa ao longo de quatro anos em conjunto com ensino médio em uma instituição de insino fora do comum. Experiência com bolsa de iniciação científica, monitoria e diveras atividades  extracurriculares fazem do Instituto Federal de Educação Ciência e Tecnologia uma instituição acima da média. </p>
                        </div>
                    </div>
                    <!--    [  End Single education Info] -->
                    <!--    [Start Single education Info] -->
                    <div class="col-sm-12">
                        <div class="single-education">
                            <div class="qualification">
                                <span class="primary-color"> Engenheiro da Computação </span>
                                <span> Unisinos </span>
                                <span> 2015 - trancada</span>
                            </div>
                            <p> Um ano e meio cursando engenharia da computação. Local onde fiz diversos aprendizados e que proporcionou uma outra visão de mundo e da área a qual atuo. Infelizmente  devido a problemas pessoais foi necessário que trancasse a mesma.</p>
                        </div>
                    </div>
                    <!--    [  End Single education Info] -->
                    <!--    [Start Single education Info] -->
                    <div class="col-sm-12">
                        <div class="single-education">
                            <div class="qualification">
                                <span class="primary-color"> Desenvolvedor Web</span>
                                <span> 2015 - 2017</span>
                            </div>
                            <p>Desde 2015 atuando diretamente no mercado como programador front-end e back-end. Experiências diversas trabalhando em empresas de pequeno, médio e grande porte, trazendo assim com elas uma vasta bagagem na área. </p>
                        </div>
                    </div>
                    <!--    [  End Single education Info] -->
                </div>
            </div>
        </div>
    </section>
    <!--    [  End Education Area] -->

    <!--    [Start Skill Area] -->
    <section class="skill-area section-padding" id="skills">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="section-title">
                        <h3>Minhas Habilidades</h3>
                        <p>Algumas das habilidades que concegui ao longo da carreira:</p>
                        <p class="skill-obs">*Valores dispostos de acordo com o que acredito ser adequado no periodo de 2 anos e meio</p>
                    </div>
                </div>
            </div>
            <div class="row marginTop">
                <div class="col-md-6">
                    <div class="skill-img">
                        <img src="assets/img/foto.jpg" alt="perfil">
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="skill-wrap">
                        <!-- [Start single skill] -->
                        <div class="single-skill" data-percent="85">
                            <p class="value">PHP</p>
                            <span>85%</span>
                            <div class="skill-bar">
                                <div class="skill-bar-overlay"></div>
                            </div>
                        </div>
                        <!-- [  End single skill] -->
                        <!-- [Start single skill] -->
                        <div class="single-skill" data-percent="65">
                            <p class="value">MySQL</p>
                            <span>65%</span>
                            <div class="skill-bar">
                                <div class="skill-bar-overlay"></div>
                            </div>
                        </div>
                        <!-- [  End single skill] -->
                        <!-- [Start single skill] -->
                        <div class="single-skill" data-percent="85">
                            <p class="value">HTML</p>
                            <span>85%</span>
                            <div class="skill-bar">
                                <div class="skill-bar-overlay"></div>
                            </div>
                        </div>
                        <!-- [  End single skill] -->
                        <!-- [Start single skill] -->
                        <div class="single-skill" data-percent="75">
                            <p class="value">CSS</p>
                            <span>75%</span>
                            <div class="skill-bar">
                                <div class="skill-bar-overlay"></div>
                            </div>
                        </div>
                        <!-- [  End single skill] -->
                        <!-- [Start single skill] -->
                        <div class="single-skill" data-percent="65">
                            <p class="value">Bootstrap</p>
                            <span>65%</span>
                            <div class="skill-bar">
                                <div class="skill-bar-overlay"></div>
                            </div>
                        </div>
                        <!-- [  End single skill] -->
                        <!-- [Start single skill] -->
                        <div class="single-skill" data-percent="60">
                            <p class="value">JQuery</p>
                            <span>60%</span>
                            <div class="skill-bar">
                                <div class="skill-bar-overlay"></div>
                            </div>
                        </div>
                        <!-- [  End single skill] -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [  End Skill Area] -->

    <!--    [Start Service Area] -->
    <section class="service-area section-padding" id="service">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="section-title">
                        <h3>Soluções</h3>
                        <p>Trabalhando com diversas áres, trago a solução ideal para cada cliente dentro de suas necessidades</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="service-wrap">
                    <!--    [Start Single Service] -->
                    <div class="col-md-3 col-sm-6">
                        <div class="single-service text-center">
                            <i class="fa fa-paint-brush"></i>
                            <h4>Desing</h4>
                            <p>Criação de identidade visual, logos,  layout e tudo mais que sua empresa precisa</p>
                        </div>
                    </div>
                    <!--    [  End Single Service] -->
                    <!--    [Start Single Service] -->
                    <div class="col-md-3 col-sm-6">
                        <div class="single-service text-center">
                            <i class="fa fa-desktop"></i>
                            <h4>Desenvolvimento Web</h4>
                            <p>Sites simples e complexos da maneira que o cliente deseja com velocidade na entrega</p>
                        </div>
                    </div>
                    <!--    [  End Single Service] -->
                    <!--    [Start Single Service] -->
                    <div class="col-md-3 col-sm-6">
                        <div class="single-service text-center">
                            <i class="fa fa-database"></i>
                            <h4>Sistemas</h4>
                            <p>Desenvolvimento e manutenção de sistemas web com agilidade e eficiecia</p>
                        </div>
                    </div>
                    <!--    [  End Single Service] -->
                    <!--    [Start Single Service] -->
                    <div class="col-md-3 col-sm-6">
                        <div class="single-service text-center">
                            <i class="fa fa-bullhorn"></i>
                            <h4>Marketimg Digital</h4>
                            <p>Prospectação de clientes e expansão do seu negócio através das midias digitais da melhor maneira possível </p>
                        </div>
                    </div>
                    <!--    [  End Single Service] -->
                </div>
            </div>
        </div>
    </section>
    <!--    [  End Service Area] -->

    <!--    [Start Portfolio Area] -->
    <section class="portfolio-area section-padding" id="portfolio">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="section-title">
                        <h3>portfólio</h3>
                        <p>Alguns dos trabalhos já realizei ou dei manutenção</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <ul class="portfolio-menu text-center">
                    <li class="current" data-filter="*">Todos</li>
                    <li data-filter=".design">Design</li>
                    <li data-filter=".desenvolvimento">Desenvolvimento Web</li>
                    <li data-filter=".wordpress">WordPress</li>
                    <li data-filter=".sistemas">Sistemas</li>
                </ul>

            </div>
            <div class="row">
                <div class="grid">
                    <!--    Start Single Portfolio Item -->
                    <div class="col-sm-8 item sistemas">
                        <div class="single-portfolio-item">
                            <div class="portfolio-img">
                                <img src="assets/img/clam.png" alt="clam">
                            </div>
                            <div class="portfolio-desc">
                                <h4>Clam Imobiliária<span>Site expositivo</span></h4>
                                <a href="assets/img/clam.png" class="portfolio-popup"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--      End Single Portfolio Item -->

                    <!--    Start Single Portfolio Item -->
                    <div class="col-sm-4 item design desenvolvimento">
                        <div class="single-portfolio-item">
                            <div class="portfolio-img">
                                <img src="assets/img/grupoplaneta.png" alt="grupoplaneta">
                            </div>
                            <div class="portfolio-desc">
                                <h4>Planeta Vip Soluções<span>Site Institucional</span></h4>
                                <a href="assets/img/grupoplaneta.png" class="portfolio-popup"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--      End Single Portfolio Item -->

                    <!--    Start Single Portfolio Item -->
                    <div class="col-sm-4 item sistemas">
                        <div class="single-portfolio-item">
                            <div class="portfolio-img">
                                <img src="assets/img/agora.png" alt="agora">
                            </div>
                            <div class="portfolio-desc">
                                <h4>Ágora Imobiliaria<span>Site expositivo</span></h4>
                                <a href="assets/img/agora.png" class="portfolio-popup"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--      End Single Portfolio Item -->

                    <!--    Start Single Portfolio Item -->
                    <div class="col-sm-4 item design desenvolvimento">
                        <div class="single-portfolio-item">
                            <div class="portfolio-img">
                                <img src="assets/img/telefonia.png" alt="">
                            </div>
                            <div class="portfolio-desc">
                                <h4>Planeta Vip Telefonia<span>Site Institucional</span></h4>
                                <a href="assets/img/telefonia.png" class="portfolio-popup"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--      End Single Portfolio Item -->

                    <!--    Start Single Portfolio Item -->
                    <div class="col-sm-4 item wordpress">
                        <div class="single-portfolio-item">
                            <div class="portfolio-img">
                                <img src="assets/img/solaris.png" alt="solaris">
                            </div>
                            <div class="portfolio-desc">
                                <h4>Solaris Corretora <span>Site Institucional</span></h4>
                                <a href="assets/img/solaris.png" class="portfolio-popup"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--      End Single Portfolio Item -->

                    <!--    Start Single Portfolio Item -->
                    <div class="col-sm-8 item sistemas">
                        <div class="single-portfolio-item">
                            <div class="portfolio-img">
                                <img src="assets/img/sublimacoes.png" alt="sublimacoes">
                            </div>
                            <div class="portfolio-desc">
                                <h4>Planeta Vip Sublimações <span>Loja Virtual</span></h4>
                                <a href="assets/img/sublimacoes.png" class="portfolio-popup"><i class="fa fa-link"></i></a>
                            </div>
                        </div>
                    </div>
                    <!--      End Single Portfolio Item -->
                </div>
            </div>

        </div>
    </section>
    <!--    [  End Portfolio Area] -->

    <!--    [Start Contact Area] -->
    <section class="contact-area section-padding" id="contact">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 text-center">
                    <div class="section-title section-white-title">
                        <h3>Entre em Contato</h3>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="contact-wrap">
                    <div class="col-md-offset-2 col-md-4">
                        <div class="contact-info">

                            <div class="single-contact-info">
                                <div class="bar"></div>
                                <h5><i class="fa fa-phone"></i> Telefone</h5>

                                (51) 98467-8003 <br> (51) 98404-2455
                            </div>

                            <div class="single-contact-info">
                                <div class="bar"></div>
                                <h5><i class="fa fa-envelope"></i> email</h5>

                                contato@antoniorolim.com.br <br> antonio@planetavipsolucoes.com.br
                            </div>

                            <div class="single-contact-info">
                                <div class="bar"></div>
                                <h5><i class="fa fa-globe"></i> Website</h5>
                                www.antoniorolim.com.br <br> www.planetavipsolucoes.com.br
                            </div>

                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="contact-form-wrap">
                            <form id="contact-form" action="assets/php/mail.php" method="post" enctype="multipart/form-data">
                                <div class="msg-box"></div>
                                <div class="row">
                                    <div class="col-sm-12 form-group">
                                        <input type="text" name="name" class="form-control" placeholder="Nome" required>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <input type="email" name="email" class="form-control" placeholder="Email" required>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <input type="text" name="subject" class="form-control" placeholder="Assunto" required>
                                    </div>
                                    <div class="col-sm-12 form-group">
                                        <textarea name="msg" class="form-control" cols="30" rows="10" placeholder="Mensagem" required></textarea>
                                    </div>
                                    <div class="col-sm-12 text-right">
                                        <button type="submit"><i class="fa fa-send"></i>Enviar</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--    [  End Contact Area] -->

    <!--    [Start Footer Area] -->
    <footer class="footer-area" id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <ul class="social-profile">
                        <li><a href="https://www.facebook.com/antoniorolim96"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/Antonio_Rolim"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://github.com/antoniorolim"><i class="fa fa-github"></i></a></li>
                        <li><a href="https://www.linkedin.com/in/antonio-rolim-67094654/?lipi=urn%3Ali%3Apage%3Ad_flagship3_feed%3B49yEi6TNQjWoH8Ok6dvUPQ%3D%3D&licu=urn%3Ali%3Acontrol%3Ad_flagship3_feed-identity_profile_photo"><i class="fa fa-linkedin"></i></a></li>
                    </ul>
                </div>
                <div class="col-sm-6 text-capitalize text-right">
                    <p>&copy; copyright templatehut | all right reserved</p>
                </div>
            </div>

            <!--    [Start Scroll To Top] -->
            <a class="scroll-to-top" href="#"><i class="fa fa-chevron-circle-up"></i></a>
            <!--    [  End Scroll To Top] -->
        </div>
    </footer>
    <!--    [  End Footer Area] -->


    <!--    [jQuery Js]-->
    <script src="assets/js/jquery-2.2.4.min.js"></script>
    <!--    [Bootstrap Js]-->
    <script src="assets/js/bootstrap.min.js"></script>
    <!--    [Animated Headline Js]-->
    <script src="assets/js/animated-headline.min.js"></script>
    <!--    [OWl Carousel Js]-->
    <script src="assets/js/owl.carousel.min.js"></script>
    <!--    [Waypoint Js]-->
    <script src="assets/js/waypoints.min.js"></script>
    <!--    [Isotope Js]-->
    <script src="assets/js/isotope.pkgd.min.js"></script>
    <!--    [Easing Js]-->
    <script src="assets/js/jquery.easing.1.3.min.js"></script>
    <!--    [Stellar Js]-->
    <script src="assets/js/jquery.stellar.min.js"></script>
    <!--    [Magnific Popup Js]-->
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <!--    [Active Js] -->
    <script src="assets/js/active.js"></script>
</body>

</html>